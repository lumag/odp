Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OpenDataPlane
Source: https://github.com/Linaro/odp.git

Files: *
Copyright: 2013-2018 Linaro Limited, All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_bitset.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_queue_if.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_queue_scalable_internal.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_schedule_scalable_config.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_schedule_scalable.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_llqueue.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_schedule_scalable_ordered.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/default/odp_cpu.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/default/odp_cpu_idling.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/arm/odp_cpu.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/arm/odp_cpu_idling.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/arm/odp_atomic.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/arm/odp_llsc.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/aarch64/odp_cpu.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/aarch64/odp_cpu_idling.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/aarch64/odp_atomic.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/arch/aarch64/odp_llsc.h
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_queue_scalable.c
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_schedule_scalable.c
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_queue_if.c
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_schedule_scalable_ordered.c
Copyright: 2017, ARM Limited. All rights reserved.
License: BSD-3-Clause

Files: example/l3fwd/odp_l3fwd_db.c
Copyright: 2006 Bob Jenkins (bob_jenkins@burtleburtle.net)
License: BSD-3-Clause

Files: test/performance/odp_pktio_ordered.c
Copyright: 2006 Bob Jenkins (bob_jenkins@burtleburtle.net)
License: BSD-3-Clause

Files: platform/linux-generic/odp_hash.c
Copyright: 2010-2013 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: test/performance/dummy_crc.h
Copyright: 2010-2013 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_system_info.c
Copyright: 2010-2014 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/odp_system_info.c
Copyright: 2010-2015 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/pktio/ring.c
Copyright: 2010-2013 Intel Corporation, All rights reserved.
           2007,2008 Kip Macy kmacy@freebsd.org
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_packet_io_ring_internal.h
Copyright: 2010-2013 Intel Corporation, All rights reserved.
           2007,2008 Kip Macy kmacy@freebsd.org
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_traffic_mngr_internal.h
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_sorted_list_internal.h
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_timer_wheel_internal.h
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_pkt_queue_internal.h
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_name_table_internal.h
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/odp_traffic_mngr.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/odp_pkt_queue.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/odp_timer_wheel.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/odp_sorted_list.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/odp_name_table.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: example/traffic_mgmt/odp_traffic_mgmt.c
Copyright: 2015 EZchip Semiconductor Ltd., All Rights Reserved
License: BSD-3-Clause

Files: platform/linux-generic/pktio/tap.c
Copyright: 2015, Ilya Maximets <i.maximets@samsung.com>
License: BSD-3-Clause

Files: platform/linux-generic/test/validation/api/pktio/pktio_run_tap.sh
Copyright: 2015, Ilya Maximets <i.maximets@samsung.com>
License: BSD-3-Clause

Files: helper/include/odp/helper/odph_cuckootable.h
Copyright: 2010-2016 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: helper/cuckootable.c
Copyright: 2010-2016 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: helper/test/cuckootable.c
Copyright: 2010-2016 Intel Corporation, All rights reserved.
License: BSD-3-Clause

Files: platform/linux-generic/include/odp_packet_socket.h
Copyright: 2013, Nokia Solutions and Networks
License: BSD-3-Clause

Files: platform/linux-generic/pktio/loop.c
Copyright: 2013, Nokia Solutions and Networks
License: BSD-3-Clause

Files: platform/linux-generic/pktio/pktio_common.c
Copyright: 2013, Nokia Solutions and Networks
License: BSD-3-Clause

Files: platform/linux-generic/pktio/socket_mmap.c
Copyright: 2013, Nokia Solutions and Networks
License: BSD-3-Clause

Files: platform/linux-generic/pktio/socket.c
Copyright: 2013, Nokia Solutions and Networks
License: BSD-3-Clause

Files: example/ipsec_offload/odp_ipsec_offload.c
Copyright: 2017 NXP. All rights reserved.
License: BSD-3-Clause

Files: example/ipsec_offload/odp_ipsec_offload_cache.c
Copyright: 2017 NXP. All rights reserved.
License: BSD-3-Clause

Files: m4/ax_check_compile_flag.m4
Copyright: 2008, Guido U. Draheim <guidod@gmx.de>
           2011, Maarten Bosmans <mkbosmans@gmail.com>
License: GPL3+-with-Autoconf-Macros-exception

Files: m4/ax_compiler_vendor.m4
Copyright: 2008, Steven G. Johnson <stevenj@alum.mit.edu>
           2008, Matteo Frigo
License: GPL3+-with-Autoconf-Macros-exception

Files: m4/ax_compiler_version.m4
Copyright: 2014 Bastien ROUCARIES <roucaries.bastien+autoconf@gmail.com>
License: GNU-All-Permissive-License

Files: m4/ax_prog_doxygen.m4
Copyright: 2009, Oren Ben-Kiki <oren@ben-kiki.org>
License: GNU-All-Permissive-License

Files: m4/ax_pthread.m4
Copyright: 2008, Steven G. Johnson <stevenj@alum.mit.edu>
           2011, Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL3+-with-Autoconf-Macros-exception

Files: m4/ax_valgrind_check.m4
Copyright: 2014-2016, Philip Withnall <philip.withnall@collabora.co.uk>
           2017, Dmitry Eremin-Solenikov
License: GNU-All-Permissive-License

Files: debian/*
Copyright: 2018 Dmitry Eremin-Solenikov <dmitry.ereminsolenikov@linaro.org>
 2016 Fathi Boudra <fathi.boudra@linaro.org>
 2015-2016 Maxim Uvarov <maxim.uvarov@linaro.org>
 2015-2016 Anders Roxell <anders.roxell@linaro.org>
License: BSD-3-Clause

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. Neither the name of the University nor the names of its contributors
    may be used to endorse or promote products derived from this software
    without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE HOLDERS OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL3+-with-Autoconf-Macros-exception
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or (at
 your option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301,
 USA.
 .
 As a special exception, the respective Autoconf Macro's copyright
 owner gives unlimited permission to copy, distribute and modify the
 configure scripts that are the output of Autoconf when processing the
 Macro. You need not follow the terms of the GNU General Public License
 when using or distributing such scripts, even though portions of the
 text of the Macro appear in them. The GNU General Public License (GPL)
 does govern all other use of the material that constitutes the
 Autoconf Macro.
 .
 This special exception to the GPL applies to versions of the Autoconf
 Macro released by the GNU Autoconf Macro Archive. When you make and
 distribute a modified version of the Autoconf Macro, you may extend
 this special exception to the GPL to apply to your modified version as
 well.
 .
 On Debian GNU/Linux systems, the complete text of the GNU General Public
 License can be found in /usr/share/common-licenses/GPL-3.

License: GNU-All-Permissive-License
 Copying and distribution of this file, with or without modification, are
 permitted in any medium without royalty provided the copyright notice
 and this notice are preserved. This file is offered as-is, without any
 warranty.
